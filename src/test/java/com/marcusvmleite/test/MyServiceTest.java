package com.marcusvmleite.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.core.Dispatcher;
import org.jboss.resteasy.mock.MockDispatcherFactory;
import org.jboss.resteasy.mock.MockHttpRequest;
import org.jboss.resteasy.mock.MockHttpResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.marcusvmleite.model.Image;
import com.marcusvmleite.model.Product;
import com.marcusvmleite.rest.MyService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/applicationContext.xml")
@Transactional
public class MyServiceTest {

	@Autowired
	private ApplicationContext context;
	
	private Dispatcher dispatcher;
	
	private ObjectMapper mapper;

	@Before
	public void setUp() {
		MyService g = new MyService();
		context.getAutowireCapableBeanFactory().autowireBean(g);
		dispatcher = MockDispatcherFactory.createDispatcher();
		dispatcher.getRegistry().addSingletonResource(g);
		mapper = new ObjectMapper();
	}

	@Test
	public void TestAllProducts() {
		MockHttpRequest request;
		try {
			request = MockHttpRequest.get("/api/allProducts");
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);
			assertTrue(response.getStatus() == 200);
		} catch (URISyntaxException e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void TestAllProductsWithoutRelationship() {
		MockHttpRequest request;
		try {
			request = MockHttpRequest.get("/api/allProductsWithoutRelationship");
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);
			assertTrue(response.getStatus() == 200);
		} catch (URISyntaxException e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void TestProduct() {
		MockHttpRequest request;
		try {
			request = MockHttpRequest.get("/api/product/1");
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);
			assertTrue(response.getStatus() == 200);
		} catch (URISyntaxException e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void TestProductWithoutRelationship() {
		MockHttpRequest request;
		try {
			request = MockHttpRequest.get("/api/productWithoutRelationship/1");
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);
			assertTrue(response.getStatus() == 200);
		} catch (URISyntaxException e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void TestProductChildren() {
		MockHttpRequest request;
		try {
			request = MockHttpRequest.get("/api/productChildren/1");
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);
			assertTrue(response.getStatus() == 200);
		} catch (URISyntaxException e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void TestImagesByProduct() {
		MockHttpRequest request;
		try {
			request = MockHttpRequest.get("/api/imagesByProduct/1");
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);
			assertTrue(response.getStatus() == 200);
		} catch (URISyntaxException e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void TestCreateImage() throws JsonGenerationException, JsonMappingException, IOException {
		MockHttpRequest request;
		try {
			request = MockHttpRequest.post("/api/image/create")
					.contentType(MediaType.APPLICATION_JSON_TYPE)
					.content(getBytes(buildImage()));
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);
			assertTrue(response.getStatus() == 200);
		} catch (URISyntaxException e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void TestCreateSimpleProduct() throws JsonGenerationException, JsonMappingException, IOException {
		MockHttpRequest request;
		try {
			request = MockHttpRequest.post("/api/product/create")
					.contentType(MediaType.APPLICATION_JSON_TYPE)
					.content(getBytes(buildSimpleProduct()));
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);
			assertTrue(response.getStatus() == 200);
		} catch (URISyntaxException e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void TestCreateProductWithParent() throws JsonGenerationException, JsonMappingException, IOException {
		MockHttpRequest request;
		try {
			request = MockHttpRequest.post("/api/product/create")
					.contentType(MediaType.APPLICATION_JSON_TYPE)
					.content(getBytes(buildProductWithParent()));
			System.out.println("DSDSDSDSDSDS: " + mapper.writeValueAsString(buildProductWithParent()));
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);
			assertTrue(response.getStatus() == 200);
		} catch (URISyntaxException e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void TestUpdateProduct() throws JsonGenerationException, JsonMappingException, IOException {
		MockHttpRequest request;
		try {
			request = MockHttpRequest.post("/api/product/update")
					.contentType(MediaType.APPLICATION_JSON_TYPE)
					.content(getBytes(buildExistingProduct()));
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);
			assertTrue(response.getStatus() == 200);
		} catch (URISyntaxException e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void TestUpdateImage() throws JsonGenerationException, JsonMappingException, IOException {
		MockHttpRequest request;
		try {
			request = MockHttpRequest.post("/api/image/update")
					.contentType(MediaType.APPLICATION_JSON_TYPE)
					.content(getBytes(buildExistingImage()));
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);
			assertTrue(response.getStatus() == 200);
		} catch (URISyntaxException e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void TestDeleteImage() {
		MockHttpRequest request;
		MockHttpResponse response;
		try {
			request = MockHttpRequest.delete("/api/image/delete/1");
			response = new MockHttpResponse();
			dispatcher.invoke(request, response);
			assertTrue(response.getStatus() == 200);
		} catch (URISyntaxException e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void TestDeleteProduct() {
		MockHttpRequest request;
		try {
			request = MockHttpRequest.delete("/api/product/delete/1");
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);
			assertTrue(response.getStatus() == 200);
		} catch (URISyntaxException e) {
			fail(e.getMessage());
		}
	}
	
	private Product buildSimpleProduct() {
		return new Product.Builder("Product Test", "Some random description...").build();
	}
	
	private Object buildExistingProduct() {
		return new Product.Builder("Updating Product Test", "Updating some random description...")
				.addId(1).build();
	}
	
	private Product buildProductWithParent() {
		return new Product.Builder("Product Test With Parent", "Some random description...")
				.addParent(buildSimpleProduct())
				.build();
	}
	
	private Image buildImage() {
		Image image = new Image();
		image.setProduct(new Product.Builder(1).build());
		image.setType("Type Test");
		return image;
	}
	
	private Object buildExistingImage() {
		Image image = new Image();
		image.setId(1);
		image.setProduct(new Product.Builder(1).build());
		image.setType("Updating Type Test");
		return image;
	}
	
	private byte[] getBytes(Object object) throws JsonGenerationException, JsonMappingException, IOException {
		return mapper.writeValueAsBytes(object);
	}
	
}
