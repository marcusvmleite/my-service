package com.marcusvmleite.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.marcusvmleite.dao.ImageDao;
import com.marcusvmleite.dao.ProductDao;
import com.marcusvmleite.model.Image;
import com.marcusvmleite.model.Product;

@Component
@Transactional
@Path("/api")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MyService {

	@Autowired
	private ProductDao productDao;
	
	@Autowired
	private ImageDao imageDao;
	
	/**
	 * Get all products with relationships.
	 * @return List of {@link Product}
	 */
	@GET
	@Path("/allProducts")
	public List<Product> allProducts() {
		return productDao.getAll();

	}
	
	/**
	 * Get all products without relationships.
	 * @return List of {@link Product}
	 */
	@GET
	@Path("/allProductsWithoutRelationship")
	public List<Product> allProductsWithoutRelationship() {
		return productDao.getAllWithoutRelationships();

	}
	
	/**
	 * Get a product of a specified id
	 * @param id Identifier
	 * @return {@link Product}
	 */
	@GET
	@Path("/product/{id}")
	public Product product(@PathParam("id") Integer id) {
		return productDao.get(id);
	}
	
	/**
	 * Get a product of a specified id, without its relationships
	 * @param id Identifier
	 * @return {@link Product}
	 */
	@GET
	@Path("/productWithoutRelationship/{id}")
	public Product productWithoutRelationship(@PathParam("id") Integer id) {
		return productDao.getProductWithoutRelationship(id);
	}
	
	/**
	 * Get all children from a specific product
	 * @param id Identifier
	 * @return List of {@link Product}
	 */
	@GET
	@Path("/productChildren/{id}")
	public List<Product> productChildren(@PathParam("id") Integer id) {
		return productDao.get(id).getChildren();
	}
	
	/**
	 * Get all images from a specific product
	 * @param id Identifier
	 * @return List of {@link Image}
	 */
	@GET
	@Path("/imagesByProduct/{id}")
	public List<Image> imagesByProduct(@PathParam("id") Integer id) {
		return productDao.get(id).getImages();
	}
	
	/**
	 * Remove a product with a specific id. All its children and
	 * images will be removed too.
	 * @param id Identifier
	 * @return {@link Response}
	 */
	@DELETE
	@Path("/product/delete/{id}")
	public Response deleteProduct(@PathParam("id") Integer id) {
		productDao.deleteById(id);
		return Response.status(200).entity("").build();
	}
	
	/**
	 * Remove a image with a specific id.
	 * @param id Identifier
	 * @return {@link Response}
	 */
	@DELETE
	@Path("/image/delete/{id}")
	public Response deleteImage(@PathParam("id") Integer id) {
		imageDao.deleteById(id);
		return Response.status(200).entity(id).build();
	}
	
	/**
	 * Create a product.
	 * @param product {@link Product}
	 * @return {@link Response}
	 */
	@POST
	@Path("/product/create")
	@Consumes("application/json")
	public Response createProduct(Product product) {
		productDao.create(product);
		return Response.status(200).entity(product).build();
	}
	
	/**
	 * Create a image of a product.
	 * @param product {@link Product}
	 * @return {@link Response}
	 */
	@POST
	@Path("/image/create")
	public Response createImage(Image image) {
		imageDao.create(image);
		return Response.status(200).entity(image).build();
	}
	
	/**
	 * Update a product.
	 * @param product {@link Product}
	 * @return {@link Response}
	 */
	@POST
	@Path("/product/update")
	public Response updateProduct(Product product) {
		productDao.update(product);
		return Response.status(200).entity(product).build();
	}
	
	/**
	 * Update a image.
	 * @param product {@link Image}
	 * @return {@link Response}
	 */
	@POST
	@Path("/image/update")
	public Response updateImage(Image image) {
		imageDao.create(image);
		return Response.status(200).entity(image).build();
	}
	
}
