package com.marcusvmleite.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "product")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class Product implements Serializable {

	private static final long serialVersionUID = -4730270166974830512L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column
	private String name;

	@Column
	private String description;

	@ManyToOne(optional = true, cascade = CascadeType.ALL)
	@JoinColumn(name = "parent")
	private Product parent;

	@OneToMany(mappedBy = "parent", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Product> children;

	@OneToMany(mappedBy = "id", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Image> images;

	public Product() {}
	
	private Product(Builder builder) {
		this.id = builder.id;
		this.name = builder.name;
		this.description = builder.description;
		this.parent = builder.parent;
		this.children = builder.children;
		this.images = builder.images;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Product getParent() {
		return parent;
	}

	public void setParent(Product parent) {
		this.parent = parent;
	}

	public List<Product> getChildren() {
		return children;
	}

	public void setChildren(List<Product> children) {
		this.children = children;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

	public static class Builder {

		private Integer id;
		private String name;
		private String description;
		private Product parent;
		private List<Product> children;
		private List<Image> images;

		public Builder(Integer id) {
			if (id == null) {
				throw new IllegalArgumentException("Id cannot be null!");
			}
			this.id = id;
		}
		
		public Builder(String name, String description) {
			if (!StringUtils.hasText(name) || !StringUtils.hasText(description)) {
				throw new IllegalArgumentException("Name and Description cannot be empty!");
			}
			this.id = null;
			this.name = name;
			this.description = description;
		}

		public Builder addParent(Product parent) {
			this.parent = parent;
			return this;
		}

		public Builder addChildren(List<Product> product) {
			this.children = product;
			return this;
		}

		public Builder addImages(List<Image> images) {
			this.images = images;
			return this;
		}
		
		public Builder addId(Integer id) {
			this.id = id;
			return this;
		}
		
		public Product build() {
	      return new Product(this);
	    }

	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", description=" + description + ", parent= " + parent != null
				? parent.getName() : "none" + "]";
	}

}
