package com.marcusvmleite.dao;

import org.springframework.stereotype.Repository;

import com.marcusvmleite.model.Image;

@Repository
public class ImageDaoImpl extends GenericDaoImpl<Image> implements ImageDao {

}
