package com.marcusvmleite.dao;

import com.marcusvmleite.model.Image;

public interface ImageDao extends GenericDao<Image> {

}
