package com.marcusvmleite.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;

public interface GenericDao<T extends Serializable> {

	T get(Serializable id);
	
	List<T> getAll();
	
	Session getSession();
	
	void update(T t);
	
	void delete(T t);
	
	void deleteById(Serializable id);
	
	void deleteAll();
	
	void create(T t);
	
}
