package com.marcusvmleite.dao;

import java.util.List;

import com.marcusvmleite.model.Product;

public interface ProductDao extends GenericDao<Product> {

	List<Product> getAllWithoutRelationships();

	Product getProductWithoutRelationship(Integer id);

}
