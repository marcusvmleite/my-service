package com.marcusvmleite.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.marcusvmleite.model.Product;

@Repository
public class ProductDaoImpl extends GenericDaoImpl<Product> implements ProductDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Product> getAllWithoutRelationships() {
		return getCriteriaSpecificColumns().list();
	}

	@Override
	public Product getProductWithoutRelationship(Integer id) {
		return (Product) getCriteriaSpecificColumns().add(Restrictions.eq("id", id)).uniqueResult();
	}
	
	private Criteria getCriteriaSpecificColumns() {
		return getSession().createCriteria(Product.class)
			    .setProjection(Projections.projectionList()
					      .add(Projections.property("id"), "id")
					      .add(Projections.property("name"), "name")
					      .add(Projections.property("description"), "description"))
					    .setResultTransformer(Transformers.aliasToBean(Product.class));
	}

}
