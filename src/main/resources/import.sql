INSERT INTO product (id, name, description, parent) VALUES (1, 'Product A', 'Description for Product A!', null);
INSERT INTO product (id, name, description, parent) VALUES (2, 'Product B', 'Description for Product B!', 1);
INSERT INTO product (id, name, description, parent) VALUES (3, 'Product C', 'Description for Product C!', 1);
INSERT INTO product (id, name, description, parent) VALUES (4, 'Product D', 'Description for Product D!', 2);
INSERT INTO product (id, name, description, parent) VALUES (5, 'Product E', 'Description for Product E!', 2);
INSERT INTO product (id, name, description, parent) VALUES (6, 'Product A', 'Description for Product A!', null);
INSERT INTO product (id, name, description, parent) VALUES (7, 'Product A', 'Description for Product A!', null);
INSERT INTO product (id, name, description, parent) VALUES (8, 'Product A', 'Description for Product A!', null);

INSERT INTO image (id, type, product) VALUES (1, 'TYPE 1', 1);
INSERT INTO image (id, type, product) VALUES (2, 'TYPE 2', 2);
INSERT INTO image (id, type, product) VALUES (3, 'TYPE 3', 3);
INSERT INTO image (id, type, product) VALUES (4, 'TYPE 4', 4);
INSERT INTO image (id, type, product) VALUES (5, 'TYPE 5', 5);